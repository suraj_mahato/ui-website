package uiwebsitecom.uiwebsite.qa.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.uiwebsite.qa.base.TestBase;
import com.uiwebsite.qa.listeners.uiwebsiteListerners;
import com.uiwebsite.qa.pages.OrderTrailsPage;
import com.uiwebsite.qa.utilities.CustomerDetails;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ uiwebsiteListerners.class })
@Feature("Order Trails functionality end to end testing")
@Epic("End To End Testing, Integration Testing, Smoke Testing")
public class OrderTrailsPageTest extends TestBase {

	String sheetNames = "customers";
	
	OrderTrailsPage orderTrailsPage;
	

	public OrderTrailsPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Browser Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		orderTrailsPage =new OrderTrailsPage();


		

	}

	/*
	 * Order Trails page will be display
	 */

	@Test(enabled=false,priority = 0,description = "Clicking on Order Trail button in banner filed")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate by Clicking on Order Trail button in banner filed")
	@Story("Story Name : To check by Clicking on Order Trail button in banner filed")
	public void validateOrderTrailsPage() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
        
        String ExpectedResult = "Order Trails page open";
        
        Assert.assertEquals("Order Trails page open", ExpectedResult, "Order trail page is not open");
		
		Thread.sleep(6000);

	}
	
	
	
	@Test(enabled=false,priority = 1,description = "Clicking one by one FAQ filed and verifying title with description")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate by Clicking one by one FAQ filed and verifying title with description")
	@Story("Story Name : To check by Clicking one by one FAQ filed and verifying title with description")
	public void clickingoneByoneFAQFiled() throws Exception {
		
        orderTrailsPage.clickonFAQFiled();
		
		Thread.sleep(6000);
		
		String expectedResult = "It should be display one by one FAQ filed description by clicking FAQ Filed.";
		String ActualResult= "It should be display one by one FAQ filed description by clicking FAQ Filed.";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 2,description = "User Enter Valid all Upper case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid all Upper case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid all Upper case in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterallUpperCaseValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("SURAJ", "8800735367");

	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	       
	        Thread.sleep(2000);
		
	        orderTrailsPage.clickOnNextButton();
	        
	        String ActualResult=orderTrailsPage.validateVerifyMobileNoTextMessage();
	        
	        Assert.assertEquals(ActualResult, "Verify Mobile Number", "OTP page is not displaying");
	        
		
		    Thread.sleep(6000);


	}
	
	@Test(enabled=false,priority = 3,description = "User Enter Valid all Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid all Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid all Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterallLowerCaseValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("suraj mahato", "8800735367");

	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	       
	        Thread.sleep(2000);
		
	        orderTrailsPage.clickOnNextButton();
	        
	        String ActualResult=orderTrailsPage.validateVerifyMobileNoTextMessage();
	        
	        Assert.assertEquals(ActualResult, "Verify Mobile Number", "OTP page is not displaying");
	        
		
		    Thread.sleep(6000);


	}
	
	
	@Test(enabled=false,priority = 4,description = "User Enter Valid both Upper and Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid both Upper and Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid both Upper and Lower case in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnteraUpperLowerCaseValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");

	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	       
	        Thread.sleep(2000);
		
	        orderTrailsPage.clickOnNextButton();
	        
	        String ActualResult=orderTrailsPage.validateVerifyMobileNoTextMessage();
	        
	        Assert.assertEquals(ActualResult, "Verify Mobile Number", "OTP page is not displaying");
	        
		
		    Thread.sleep(6000);


	}
	

	@Test(enabled=false,priority = 5,description = "User Enter Valid large text in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid large text in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid large text in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnteralargeLowerCaseValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("sgsgsgsgsgsgsjshshshshsjssjsjjsjsjsjjsjsjsjjsjsjsjsjsjjsjsjsjsjsjsjsjsjsjjssjsjsjsjssjssjsj", "8800735367");

	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	       
	        Thread.sleep(2000);
		
	        orderTrailsPage.clickOnNextButton();
	        
	        String ActualResult=orderTrailsPage.validateVerifyMobileNoTextMessage();
	        
	        Assert.assertEquals(ActualResult, "Verify Mobile Number", "OTP page is not displaying");
	        
		
		    Thread.sleep(6000);


	}
	

	@Test(enabled=false,priority = 6,description = "User Enter Invalid Lower case with numeric value in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid Lower case with numeric value in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Invalid Lower case with numeric value in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterinValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("suraj12345", "8800735367");

	        String ActualResults=orderTrailsPage.validationMessageofNameFiled();
	        
	        Assert.assertEquals(ActualResults, "Please enter alphabets only", "validation message not display");
	          
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);


	}
	

	@Test(enabled=false,priority = 7,description = "User Enter Invalid Lower case with special character in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid Lower case with special character in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Invalid Lower case with special character in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterspecialcharacterinValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("suraj_mahato", "8800735367");

	        String ActualResults=orderTrailsPage.validationMessageofNameFiled();
	        
	        Assert.assertEquals(ActualResults, "Please enter alphabets only", "validation message not display");
	          
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);


	}
	

	@Test(enabled=false,priority = 8,description = "User Enter Invalid means Blank text in Name Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid means Blank text in Name Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User User Enter Invalid means Blank text in Name Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterBlankinValidName() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("", "8800735367");

	        String ActualResults=orderTrailsPage.validationMessageofNamesFiled();
	        
	        Assert.assertEquals(ActualResults, "Name is required", "validation message not display");
	          
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);


	}
	
	

	@Test(enabled=false,priority = 9,description = "User Enter Valid 10 digits mobile number i.e mobile number is new in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid 10 digits mobile number i.e mobile number is new in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid 10 digits mobile number i.e mobile number is new in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterValidPhone() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");
	          
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);


	}
	
	@Test(enabled=false,priority = 10,description = "User Enter Valid 10 digits mobile number i.e mobile number is existing in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Valid 10 digits mobile number i.e mobile number is existing in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Valid 10 digits mobile number i.e mobile number is existing in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterExistingValidPhone() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "9515321546");
	          
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
		    Thread.sleep(2000);

	        orderTrailsPage.clickOnNextButton();
	        
            String ActualResult="You are not eligible. We are really sorry!";
	        
	        String ExpectedResult="You are not eligible. We are really sorry!";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Popup message is not display");
	        	
		    Thread.sleep(3000);	        

		    

	}
	
	@Test(enabled=false,priority = 11,description = "User Enter Invalid 9 digits mobile number  in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid 9 digits mobile number  in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User User Enter Invalid 9 digits mobile number  in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterinValidPhone() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "880073536");
        
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
            String ActualResults=orderTrailsPage.validationMessageofMobileFiled();
	        
	        Assert.assertEquals(ActualResults, "Enter 10 digit mobile number", "validation message not display");
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);    

		    

	}
	
	@Test(enabled=false,priority = 12,description = "User Enter Invalid alphabet character in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid alphabet character in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Invalid alphabet character in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterinValidAlphabetCharacterInMobileNumber() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "jhsgsgsgsg");
        
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
            String ActualResults=orderTrailsPage.validationMessageofMobileFiled();
	        
	        Assert.assertEquals(ActualResults, "Enter 10 digit mobile number", "validation message not display");
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);    

		    

	}
	
	@Test(enabled=false,priority = 13,description = "User Enter Invalid menas Blank digits in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter Invalid menas Blank digits in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Enter Invalid menas Blank digits in Mobile Number Filed and remaining all mandatory filed and click on Next Button")
	public void ValidateEnterBlankinValidMobileNumber() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "");
        
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
            String ActualResults=orderTrailsPage.validationMessageofMobileNoFiled();
	        
	        Assert.assertEquals(ActualResults, "Mobile number is required", "validation message not display");
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);    

		    

	}
	
	
	@Test(enabled=false,priority = 14,description = "User Select City and Locality and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Select City and Locality and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Select City and Locality and remaining all mandatory filed and click on Next Button")
	public void ValidateselectingCityLocality() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");
        
	        orderTrailsPage.validateselectinganyRandomCity();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        Thread.sleep(2000);
			
	        orderTrailsPage.clickOnNextButton();
	        
	        String ActualResult=orderTrailsPage.validateVerifyMobileNoTextMessage();
	        
	        Assert.assertEquals(ActualResult, "Verify Mobile Number", "OTP page is not displaying");
	        
		
		    Thread.sleep(6000);   

		    

	}
	
	
	@Test(enabled=false,priority = 15,description = "User Select City only and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Select City only and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User Select City only and remaining all mandatory filed and click on Next Button")
	public void ValidateselectingCityOnly() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");
	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.validateselectinganyRandomCity();
       	        
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        Thread.sleep(2000);
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);

		    

	}
	
	@Test(enabled=false,priority = 16,description = "User not Select City and Locality and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User not Select City and Locality and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User not Select City and Locality and remaining all mandatory filed and click on Next Button")
	public void ValidateWithoutselectingCityLocality() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");
	            
	        Thread.sleep(3000);
	        
	        orderTrailsPage.clickOnbuffaloMIlkAddButton();
	        
	        Thread.sleep(2000);
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);

		    

	}
	
	@Test(enabled=false,priority = 17,description = "User not Select Product like Cow milk with qty=1 and remaining all mandatory filed and click on Next Button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User not Select Product like Cow milk with qty=1 and remaining all mandatory filed and click on Next Button")
	@Story("Story Name : To check User not Select Product like Cow milk with qty=1 and remaining all mandatory filed and click on Next Button")
	public void ValidateWithoutselectinganyProduct() throws Exception {
		
		    orderTrailsPage.clickOnOrderTrailsButton();
			
			Thread.sleep(20000);
			
	        orderTrailsPage.customerDetails("Suraj Mahato", "8800735367");
	        
	        Thread.sleep(2000);
	        
	        orderTrailsPage.validateselectinganyRandomCity();
	            
	        Thread.sleep(2000);
	        
	        orderTrailsPage.validateselectinganyRandomLocality();
	        
            String ActualResult="Next Button is invisible";
	        
	        String ExpectedResult="Next Button is invisible";
	        
	        Assert.assertEquals(ActualResult, ExpectedResult, "Next Button is visible");
	        	
		    Thread.sleep(3000);

		    

	}
	
	@Test(enabled=false,priority = 18,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP ---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP ---> Finally click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP ---> Finally click on Next Button.")
	public void ValidateEnterValidOTP() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        String ActualResult=orderTrailsPage.OpenAddressPage();
        
        Assert.assertEquals(ActualResult, "Delivery Address", "Delivery Address page is not displaying");
        
        Thread.sleep(8000);

		    

	}
	

	@Test(enabled=false,priority = 19,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter invalid OTP ---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter invalid OTP ---> Finally click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter invalid OTP ---> Finally click on Next Button.")
	public void ValidateEnterinValidOTP() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "9");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        String ActualResult="Mobile number verification failed.";
        
        String ExpectedResult="Mobile number verification failed.";
        
        Assert.assertEquals(ActualResult, ExpectedResult, "Mobile number verification failed is not displayed");
        
        Thread.sleep(5000);

		    

	}
	
	
	@Test(enabled=false,priority = 20,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button.")
	public void ValidateEnterinValidOTPs() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(60000);
    	
        orderTrailsPage.clickOnNextButton();
        
        String ActualResult="Mobile number verification failed.";
        
        String ExpectedResult="Mobile number verification failed.";
        
        Assert.assertEquals(ActualResult, ExpectedResult, "Mobile number verification failed is not displayed");
        
        Thread.sleep(5000);

		    

	}
	
	@Test(enabled=false,priority = 21,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button ---> Finally Enter valid OTP and click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button ---> Finally Enter valid OTP and click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP after 1 minute ---> Then validation message show and click on Resend Button ---> Finally Enter valid OTP and click on Next Button.")
	public void ValidateClickingOnResendLink() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(60000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(5000);
        
        orderTrailsPage.clickonResendLink();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
        
        orderTrailsPage.clickOnNextButton();
        
        String ActualResult=orderTrailsPage.OpenAddressPage();
        
        Assert.assertEquals(ActualResult, "Delivery Address", "Delivery Address page is not displaying");
        
        Thread.sleep(5000);

		    

	}
	
	
	@Test(enabled=false,priority = 22,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and keep landmark Blank ---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and keep landmark Blank ---> Finally click on Next Button.")
	@Story("Story Name : To check User User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and keep landmark Blank ---> Finally click on Next Button.")
	public void ValidateEnterFlatNo() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(8000);
        
        orderTrailsPage.EnterDeliveryAddress("Om pg hostel", "Room-no: 100, jmd megapolis", "");
        
        Thread.sleep(2000);

        orderTrailsPage.clickOnNextButton();
		  
        Thread.sleep(5000);

	}
	
	@Test(enabled=false,priority = 23,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark ---> Finally click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark ---> Finally click on Next Button.")
	public void ValidateEnterFlatNoLandmark() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(8000);
        
        orderTrailsPage.EnterDeliveryAddress("Om pg hostel", "Room-no: 100, jmd megapolis", "Near by station");
        
        Thread.sleep(2000);

        orderTrailsPage.clickOnNextButton();     
        
        String ActualResult=orderTrailsPage.paymentSummaryPage();
        
        Assert.assertEquals(ActualResult, "Payment Summary", "Payment Summary page is not displaying");
		  
        Thread.sleep(5000);

	}
	
	@Test(enabled=false,priority = 24,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter location and also enter landmark---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter location and also enter landmark ---> Finally click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter location and also enter landmark ---> Finally click on Next Button.")
	public void ValidateEnterLocationandLandmarkOnly() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(8000);
        
        orderTrailsPage.EnterDeliveryAddress("Om pg hostel", "", "Near by station");  
        
        String ActualResult=orderTrailsPage.validationMessageofFlatNoFiled();
        
        Assert.assertEquals(ActualResult, "Required", "validation message is not displaying");
		  
        Thread.sleep(5000);

	}
	
	@Test(priority = 25,description = "User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark---> Finally click on Next Button.")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark ---> Finally click on Next Button.")
	@Story("Story Name : To check User Enter text in all mandatory filed and also select the product like cow milk with qty=1 and click on Next Button ---> Enter valid OTP --->click on Next Button. ---> Enter FlatNO and also enter landmark ---> Finally click on Next Button.")
	public void ValidateEnterFlatNoandLandmark() throws Exception {
		
        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");
        
        Thread.sleep(2000);

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(8000);
        
        orderTrailsPage.EnterDeliveryAddress("", "RoomNo: 100, Tikri village", "Near by station");
        
        Thread.sleep(2000);

        orderTrailsPage.clickOnNextButton();     
        
        String ActualResult=orderTrailsPage.validationMessageofFlatNoFiled();
        
        Assert.assertEquals(ActualResult, "Required", "validation message is not displaying");
		  
        Thread.sleep(5000);

	}
	
	
	@DataProvider
	public Object[][] cdTestData() {
		Object data[][] = CustomerDetails.getTestData(sheetNames);
		return data;

	}
	
	
	

	/*
	 * Integration Testing
	 */
	
	@Test(enabled=false,priority = 1,dataProvider = "cdTestData", description = "Verifying that order place functionality")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: Verifying order trail functionality with enter valid data in all manadatory filed")
	@Story("Story Name : To check and Verify order trail functionality with enter valid data in all manadatory filed")
	public void customerDetails(String Name, String MobileNo) throws Exception {

        orderTrailsPage.clickOnOrderTrailsButton();
		
		Thread.sleep(20000);
		
        orderTrailsPage.customerDetails("Suraj", "8800735367");

        orderTrailsPage.validateselectinganyRandomCity();
        
        Thread.sleep(3000);
        
        orderTrailsPage.validateselectinganyRandomLocality();
        
        Thread.sleep(3000);
        
        orderTrailsPage.clickOnbuffaloMIlkAddButton();
       
        Thread.sleep(2000);
	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.EnterOtp("1", "2", "4", "6", "8");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(8000);
        
        orderTrailsPage.EnterDeliveryAddress("testing", "test", "test");
        
        Thread.sleep(2000);
    	
        orderTrailsPage.clickOnNextButton();
        
        Thread.sleep(2000);
        
        orderTrailsPage.clickonpayNowButton();
        
        
        
	}
	
	

}