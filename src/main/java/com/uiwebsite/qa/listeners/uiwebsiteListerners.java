package com.uiwebsite.qa.listeners;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.uiwebsite.qa.base.TestBase;

import io.qameta.allure.Attachment;

public class uiwebsiteListerners implements ITestListener {

	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}
	
	//Test attachment for Allure
	
	@Attachment(value="Page screenshot" , type="image/png")
	public byte[]saveScreenshotPNG(WebDriver driver){
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	}
	
	//Test attachment for Allure
	@Attachment(value="{0}",type="text/plain")
	public static String saveTextLog(String message) {
		return message;
	}
	
	
	//HTML attachments for Allure
	@Attachment(value="{0}",type="text/html")
    public static String attachHtml(String html) {
		return html;
	}
	
	@Override
	public void onStart(ITestContext iTestContext) {
		System.out.println("I am in onStart Method "+ iTestContext.getName());
		iTestContext.setAttribute("Webdriver", TestBase.getDriver());
	}
	
	@Override
	public void onFinish(ITestContext iTestContext) {
		System.out.println("I am in onFinish Method "+ iTestContext.getName());
	}
	
	@Override
	public void onTestStart(ITestResult iTestResult) {
		System.out.println("I am in OnTest start Method "+ getTestMethodName(iTestResult) + "start");
		
	}
	
	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		System.out.println("I am in OnTest sucess Method "+ getTestMethodName(iTestResult) + "succeed");
		
	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		
		System.out.println("I am in onTestFailure Method " + getTestMethodName(iTestResult) + " Failed");
		Object testClass=iTestResult.getInstance();
		WebDriver driver=TestBase.getDriver();
		
		//Allure ScreenshotRobot and saveTextLog
		
		if(driver instanceof WebDriver) {
			System.out.println("Screenshot captured for Test case:"+ getTestMethodName(iTestResult));
			saveScreenshotPNG(driver);
		}
		
		// save Log in Allure
		
		saveTextLog(getTestMethodName(iTestResult)+ " failed and screenshot taken!");
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	

	

}
