package com.uiwebsite.qa.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.uiwebsite.qa.base.TestBase;
import io.qameta.allure.Step;


public class OrderTrailsPage extends TestBase {
	

	//Page Factory OR:
	
	@FindBy(xpath="//*[@id='slick-slide02']/div/div/div[2]/div[4]/button/span")
	WebElement OrderTrailButton;
	
	@FindBy(xpath = "//input[@placeholder='Name']")
	WebElement name_xpath;

	@FindBy(xpath = "//input[@placeholder='Mobile number']")
	WebElement mobileNo_xpath;
	
	@FindBy(xpath="//input[@placeholder='Select city']")
	WebElement selectCityXpath;
	
	@FindBy(xpath="//input[@placeholder='Select locality']")
	WebElement selectLocalityXpath;
	
	@FindBy(xpath="//*[@class='next']")
	WebElement nextButtonXpath;
	
    @FindBy(xpath="/html/body/app-root/app-trial-scheduler/div/div/app-trial-desktop/div/div[1]/div[2]/div[1]/div[2]/div/span")
    WebElement buffalomilkAddButtonXpath;
	
    @FindBy(xpath = "//*[@id='otp1Focus']")
	WebElement EnterFirstlineOtp;

	@FindBy(xpath = "//*[@id='otp2Focus']")
	WebElement EntersecondlineOtp;

	@FindBy(xpath = "//*[@id='otp3Focus']")
	WebElement EnterThirdlineOtp;

	@FindBy(xpath = "//*[@id='otp4Focus']")
	WebElement EnterFourthlineOtp;

	@FindBy(xpath = "//*[@id='otp5Focus']")
	WebElement EnterFifthlineOtp;
	
	@FindBy(xpath="/html/body/app-root/app-trial-scheduler/div/div/app-trial-desktop/div/div[2]/div[1]/div[3]/div[3]/span")
	WebElement particularDateXpath;
	
	@FindBy(xpath="//*[@class=('mat-button-wrapper')  and text() = 'Resend OTP']")
	WebElement resendOtpXpath;
	
	@FindBy(xpath="//input[@placeholder='Enter location']")
	WebElement enterLocationXpath;
	
	@FindBy(xpath="//input[@placeholder='Enter flat no / house no / building']")
	WebElement enterFlatNoXpath;

	@FindBy(xpath="//input[@placeholder='Optional']")
	WebElement enterlandmarkXpath;
	
	@FindBy(xpath="//*[@class=('next')  and text() = 'Pay Now']")
	WebElement payNowButtonXpath;
	
	@FindBy(xpath="//*[@class='ncd-trial-quantity-']")
	WebElement plusIconXpath;
	
	@FindBy(xpath="//*[@class='cd-normal-txt']")
	WebElement faqfiledXpath;
	
	@FindBy(xpath="//*[@class=('trial-header-1 mt-40')  and text() = 'Verify Mobile Number']")
	WebElement verifyMobiletextXpath;
	
	@FindBy(xpath="//*[@class=('mat-error ng-star-inserted')  and text() = ' Please enter alphabets only ']")
	WebElement validationmessageofNameFiledXpath;
	
	@FindBy(xpath="//*[@class=('mat-error ng-star-inserted')  and text() = ' Name is ']")
	WebElement validationmessageofNamesFiledXpath;
	
	@FindBy(xpath="//*[@class=('mat-error ng-star-inserted')  and text() = ' Enter 10 digit mobile number ']")
	WebElement validationmessageofMobileFiledXpath;
	
	@FindBy(xpath="//*[@class=('mat-error ng-star-inserted')  and text() = ' Mobile number is ']")
	WebElement validationmessageofMobileNoFiledXpath;
	
	@FindBy(xpath="//*[@class=('trial-header-1 mt-40')  and text() = 'Delivery Address']")
	WebElement addressPageTextXpath;
	
	@FindBy(xpath="//*[@class=('trial-header-1 mt-20 mb-20')  and text() = 'Payment Summary']")
	WebElement paymentXpath;
	
	@FindBy(xpath="//*[@class=('mat-error ng-star-inserted')]")
	WebElement validationmessageofFlatNoXpath;
	
	
	// Initializing the page objects
		public OrderTrailsPage() {
			
			PageFactory.initElements(driver, this);
		}
		
		
		//Actions:
		
		
		
		@Step("click on order trails button in banner filed")
		public void clickOnOrderTrailsButton() throws Exception {
			

			// click on order trails button
			OrderTrailButton.click();
						
		}

		
		// Customer Details:

		@Step("Verify Login Credentials Details with Username and password: {0} ")
		public void customerDetails(List<String> elements) throws Exception {

			// Array list for customer Details
			List<WebElement> xpaths = new ArrayList();

			// UserName section Xpath
			xpaths.add(name_xpath);

			// Password section Xpath
			xpaths.add(mobileNo_xpath);

			// creating Actions class
			Actions a = new Actions(driver);
			int i = 0;

			// creating for loop for Customer Details
			for (WebElement element : xpaths) {

				// clicking on element object
				element.click();

				// Get the element from the array list
				String eleString = elements.get(i);

				// Enter a text
				a.sendKeys(eleString);

				// click on enter keyword
				a.sendKeys(Keys.ENTER);

				a.perform();

				i += 1;

			}

		}
		
		
		@Step("Verify Customer Details with Name: {0} , Mobile No: {1} ")
		public void customerDetails(String Name, String Mobile_No)
				throws Exception {

			name_xpath.click();
			Actions a = new Actions(driver);
			a.sendKeys(Name);
			a.sendKeys(Keys.ENTER);
			a.perform();
			
			mobileNo_xpath.click();
			Actions m = new Actions(driver);
			m.sendKeys(Mobile_No);
			m.sendKeys(Keys.ENTER);
			m.perform();	
			
			
		}
		

		@Step("Select randomly any City")
		public void validateselectinganyRandomCity() throws Exception {
			
			selectCityXpath.click();
			
			   //list methods
			List<WebElement> elements = driver.findElements(
					By.xpath("/html/body/div[2]/div/div/div/mat-option/span"));

			System.out.println(elements.size());

			Random rnd = new Random();

			int rndInt = rnd.nextInt(elements.size());

			System.out.println("Random Number : " + rndInt);

			// select randomly any Route with any index wise
			elements.get(rndInt).click();
			
			
		}
		
		

		@Step("Select randomly any Locality")
		public void validateselectinganyRandomLocality() throws Exception {
			
			selectLocalityXpath.click();
			
			   //list methods
			List<WebElement> elements = driver.findElements(
					By.xpath("/html/body/div[2]/div/div/div/mat-option/span"));

			System.out.println(elements.size());

			Random rnd = new Random();

			int rndInt = rnd.nextInt(elements.size());

			System.out.println("Random Number : " + rndInt);

			// select randomly any Route with any index wise
			elements.get(rndInt).click();
			
			
		}
		
		@Step("click on next button")
		public void clickOnNextButton() throws Exception {
			
			nextButtonXpath.click();
			
		}
		
		@Step("click on buffalo milk add button")
		public void clickOnbuffaloMIlkAddButton() throws Exception {
			
			buffalomilkAddButtonXpath.click();
			
		}
		
		@Step("Enter OTP Number OTP No-0: {0},OTP No-1: {1},OTP No-2: {2},OTP No-3: {3} ,OTP No-4: {4} ")
		public void EnterOtp(String Otp1, String Otp2, String Otp3, String Otp4, String Otp5) throws Exception {

			Thread.sleep(2000);
			EnterFirstlineOtp.sendKeys(Otp1);
			Thread.sleep(2000);
			EntersecondlineOtp.sendKeys(Otp2);
			Thread.sleep(2000);
			EnterThirdlineOtp.sendKeys(Otp3);
			Thread.sleep(2000);
			EnterFourthlineOtp.sendKeys(Otp4);
			Thread.sleep(2000);
			EnterFifthlineOtp.sendKeys(Otp5);
			Thread.sleep(2000);



		}
		
		@Step("click on resend otp link")
		public void clickonResendLink() throws Exception {
			
			resendOtpXpath.click();
			
		}
		
		@Step("click on particular date")
		public void clickonparticularDate() throws Exception {
			
			particularDateXpath.click();
			
		}
		
		@Step("click on particular date")
		public void EnterDeliveryAddress(String location, String FlatNo, String landmark) throws Exception {
			
			enterLocationXpath.click();
			
			Thread.sleep(2000);

			enterLocationXpath.clear();
			
			enterLocationXpath.sendKeys(location);
			
			Thread.sleep(2000);
			
			enterFlatNoXpath.sendKeys(FlatNo);
			
			Thread.sleep(2000);
			
			enterlandmarkXpath.sendKeys(landmark);

			
		}
		
		@Step("click on payNow button")
		public void clickonpayNowButton() throws Exception {
			
			payNowButtonXpath.click();
			
		}
		
		@Step("Click on FAQ First filed")
		
		public void clickonFAQFiled() throws Exception {
		
			//scroll Down logic
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			jse.executeScript("window.scrollBy(0,2200)");
			Thread.sleep(2000);		

			// finding list of FAQ filed
			List<WebElement> elements = driver.findElements(
					By.xpath("//*[@class='cd-normal-txt']"));
			
			System.out.println("list count:" + elements.size());

			for (int i = 0; i < elements.size(); i++) {

				// click on horizontal text by one by one
				elements.get(i).click();
				
				Thread.sleep(2000);
				
				System.out.println("Index of i is :-" + Integer.toString(i));
				

				// FAQ wise section xpath
			
				String titlexpath="//*[@id=\"mat-expansion-panel-header-" + i +"\"]/span/mat-panel-title";
				
				String descriptionXpath="//*[@id=\"cdk-accordion-child-" + i +"\"]/div/p";
				
				WebElement nameText = driver.findElement(By.xpath(titlexpath));
				
				WebElement desText = driver.findElement(By.xpath(descriptionXpath));


				// printing title with description of FAQ 
				System.out.println("FAQ Title Name : " + nameText.getText().toString());
				
				System.out.println("FAQ Description : " + desText.getText().toString());
					
			
			
		   }
		
		}
			
		@Step("Validate that validation message ")
		public String validateVerifyMobileNoTextMessage() throws Exception {

			return verifyMobiletextXpath.getText();
				   
		
		}
		
		
		@Step("Validate that validation message of name filed ")
		public String validationMessageofNameFiled() throws Exception {

		    return validationmessageofNameFiledXpath.getText();
		
		}
		
		@Step("Validate that validation message of name filed ")
		public String validationMessageofNamesFiled() throws Exception {

		    return validationmessageofNamesFiledXpath.getText();
		
		}
		
		@Step("Validate that validation message of mobile number filed ")
		public String validationMessageofMobileFiled() throws Exception {

		    return validationmessageofMobileFiledXpath.getText();
		    
		
		}
		
		@Step("Validate that validation message of mobile number filed ")
		public String validationMessageofMobileNoFiled() throws Exception {

		    return validationmessageofMobileNoFiledXpath.getText();
		    
		
		}
		
		@Step("Validate that Address page displayed ")
		public String OpenAddressPage() throws Exception {

		    return addressPageTextXpath.getText();
		    
		
		}
		
	
		@Step("Validate that payment summary screen ")
		public String paymentSummaryPage() throws Exception {

		    return paymentXpath.getText();
		    
		
		}
		

		@Step("Validate that validation message of falt no filed ")
		public String validationMessageofFlatNoFiled() throws Exception {

		    return validationmessageofFlatNoXpath.getText();
		    
		
		}
		
}
