package com.uiwebsite.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBase {

	public static ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();

	public static WebDriver driver = null;
	public static Properties prop;

	public TestBase() {

		try {

			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					"/home/dell/eclipse-workspace/uiwebsite/src/main/resources/properties/Config.properties");

			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	public WebDriver initiallization() {

		// Singleton Pattern

		if (driver == null) {

			if (prop.getProperty("browser").equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						"/home/dell/Downloads/chromedriver_linux64 (2)/chromedriver");

				driver = new ChromeDriver();

			}

			else if (prop.getProperty("browser").equalsIgnoreCase("FF")) {

				System.setProperty("webdriver.gecko.driver", "/home/dell/Downloads/geckodriverfloder/geckodriver/");

				driver = new FirefoxDriver();

			}

		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(prop.getProperty("implicitWait")), TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));

		tdriver.set(driver);
		return getDriver();

	}

	public static synchronized WebDriver getDriver() {
		return tdriver.get();
	}

}
